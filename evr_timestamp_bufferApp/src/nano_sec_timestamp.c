#include <stdio.h>
#include <stdlib.h>
#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <dbDefs.h>
#include <dbFldTypes.h>
#include <dbAccess.h>
#include <link.h>
#include <string.h>
#include <sys/time.h>



static int nano_second_timestamp(aSubRecord *prec) {
 
    /* INPA and OUTA fields are accessed by precord->a and precord->vala*/
    /* epicsInt64 or epicsUInt64 */
    epicsTimeStamp tick;

    if(!dbGetTimeStamp(&(prec->inpa),&tick)){
        /* Constant to convert from EPICS to Unix epoch (20*365.25*24*3600 = 631152000) */
        const long epics2unixEpochFactor = 631152000;

        /* Store epoch and nano seconds from EPICS timestamp */
        epicsUInt64 varSecPastEpoch  = ((epicsUInt64)tick.secPastEpoch);
        epicsUInt64 varNSec          = ((epicsUInt64)tick.nsec);
        epicsUInt64 varUnixEpoch     = 0;
        /* Store epoch converted from EPICS to Unix */
        if (varSecPastEpoch > 0 ) {
            varUnixEpoch = (varSecPastEpoch + epics2unixEpochFactor);
        }

        /*  VALA = Number of seconds since UNIX epoch started
            VALB = nanoseconds
            CALC = INT64, number of nanoseconds since UNIX epoch started
        */
        *(long *)prec->vala = (long) varUnixEpoch; //Unix epoch
        *(long *)prec->valb = (long) varNSec;
        *(epicsUInt64 *)prec->valc =  varUnixEpoch * 1000000000 + varNSec;
        return 0;
    } else {
        printf("Could not retrieve timestamp \n");
        return -1;
    }

}
 
/* Note the function must be registered at the end!*/
epicsRegisterFunction(nano_second_timestamp);
